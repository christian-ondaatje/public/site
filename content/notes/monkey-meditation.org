#+title: ...on monkey meditation
#+date: 2023-03-30

March 30, 2023

* Introduction

- turns out, if you focus on one thing and one thing only, it starts a process that ends with 'tripping balls' as they say.
  No drugs required.
  - many terms for this across many traditions:
    - the 'psychedelic', 'transcendental', or 'religious' experience
    - bliss, ecstasy, superconsciousness
    - nonduality, Nirvana, entering the Dao

     
  - Choose whatever term or tradition motivates you - it's all the same goal. For me, I like 'tripping balls.'

   
- this Focus is most difficult
- three phases of meditation in the literature
  - Dharana: concentration
  - Dhyana: meditative state
  - Samadhi: ~???~
  - I can't really speak on Dhyana or Samadhi, so these notes will explore Dharana and one guy's half-baked method of reliably getting there.
  - my understanding is that if you get good enough at Dharana, Dhyana and Samadhi will come with continued practice. But that's above my paygrade

   
- deep, life-improving benefits are manifold, but mostly it's just fun.
  (fun, but damned difficult and frustrating to get there.)
 
- this does not please the Ape
  - much as the Human would like to think it's in charge, ultimately the Ape steers the long-term ship, one short-term decision at a time

    {{< image "/images/captain-capuchin.png" "35%" "my brain, in disguise as a responsible captain" >}}
   

- therefore, in getting to the fun part (past the part that feels like Work, which no Ape will abide without proper bribery)... *one must first negotiate with the monkey.*



* How to negotiate
a) know what you want
b) find what they want
c) reliably offer enough of what they want, link it to what you want
d) behave appropriately (e.g. polite, meek, scary, provocative, etc.) so as not to ruin the deal in the long run


* Know what you want

- For me:
  - I've come to terms with the fact that deep down I am just a Junkie
    - once a junkie knows a junk, the full focus of life becomes hitched to attaining that high
    - this is an enormously powerful force, that usually ends in destruction
    - if carefully respected and balanced, however, this force can be harnessed and redirected constructively
     
  - word on the street (for the past few millennia) is that meditation brings the greatest high
  - I want.
   
- For you:
  - will take some introspection, I cannot possibly know what motivates you


* Find what they want

- in this case (and in almost all cases) what I want as the Human is just a prefrontal convolution of what the Ape has wanted all along: instant and never-ending gratification
  - if the gratification must end, then (as a hack) let me forget the concept of time
  - if you cannot remove all non-gratifying activities, then the Ape will press to bring gratification into previously non-gratifying activities.
    - This can be dangerous (cocaine at work) or deeply special (Zen dish-cleaning)

     
- all our lofty ideals and Sapien thoughtforms are supposedly just expressions of desires


* Link it to what you want

- desires are procured and managed by the Ape, with scant oversight

{{< image "/images/business-ape.png" "35%" "my brain, in disguise as a businessman" >}}

- 'motivated and stupid' is, however, the most dangerous of all combinations
- left untrained, the Ape (lacking the faculties for long-term planning) will simply do
  the exact last thing that got it high - over and over with no vision for finer herb
  - for some that's alcohol or narcotics, for others it's combat sports, many get there through psychedelics, and most of us dope digitally
  - if the Human is inattentive and the Ape is un-gratified, it will sneak into the control room and hijack faculties of higher consciousness
    - example: "I will work my ass off this evening so tomorrow I can wake up and get shit-high without anybody calling me"
    - example: "I only seem to feel comfortable getting black-out drunk with my college friends...I will plan a vacation (using opposable thumbs and capacity for written language) so that I can comfortably get black-out drunk again"
    - example: "My mind is a terrifying horrorscape at all times, except when someone is trying to kill me (and failing). I will dedicate years to the practice of Martial Art so that I can experience minutes of blissful peace in a cage"

     
- therefore, careful guidance (and control monitoring) are required.
  - Mostly though - if you keep your Simian co-captain chemically balanced it will stay out of the bridge, happily stoned in the engine room.

    {{< image "/images/engine-lemur.png" "35%" "my brain, balancing in the engine room" >}}
   

  - Problem is, 'chemically balanced' individuals seem to be our civilization's least common product.

   
- the Ape will never understand what it has not experienced
  - no use telling a monkey about meditation
  - very hard to do bribery if reward is incomprehensible or unbelievable

   
- If the Ape does not believe, it will continue to 'smoke bad dope' with predictable outcomes
  - must give the reward first, then can use the reward as a bribe in the future
  - but reward is very difficult to find without sufficient motive: chicken-egg problem
  - most practical way to break this deadlock is through psychedelics
    - psylocibin mushrooms
      - with proper research and dosage, or in-person guidance
      - most reliable way to have meditation 'done to you'
      - not to be trifled with
    - full-spectrum live resin cannabis extract, open source ("good weed")
      - cannot yet reccommend
    - full-spectrum Non-decarboxylated raw hemp extract ("CBD")
      - reccommended under very specific conditions, more on this later

       
- OF UTMOST IMPORTANCE: the reward is not the drug. the reward is the meditative state, learned with the assistance of medicine.



* Offer enough of what the Ape wants

- Drug vs. Medicine
  - Drug: used for its own purpose, instant gratification, long-term damaging, dose is stable or increasing over time.
  - Medicine: used to help you improve your long-term health, dose eventually reduced to zero.
  - anything can be either. Be a grown up and recognize which you're taking.


- Be realistic: will your body and your mind agree to just sit still and Focus on your diaphragm indefinitely without any guaranteed reward?
  - if you have not done this regularly and recently, the answer appears to be no.
  - you can fix this by guaranteeing the minimum reward necessary to produce the desired behaviour


- suggested minimal reward: 25mg BlueBird Botanicals 'Complete' CBD, taken exclusively for the purpose of producing and maintaining Dharana.
  - After two years of meticulous dosage tracking and recording of self-experimental results, I cannot confidently reccommend any other use case for CBD. I also cannot confidently reccommend dosage, but much of the literature surrounding autistic CBD dosage seems to fall in the range of several hundred milligrams.[fn:1] Intuitively, I expect the minimum reward for other people to fall somewhere well above the colloquial 10mg standard, but only you know your monkey.

   
- start small (but not too small) and if the minimum reward does not produce desired behaviour (Dharana), it is implicitly not the minimum reward and can be doubled as part of the calibration process. Remember, one must negotiate with the monkey. Your behaviour (in retrospect) is the ultimate measuring stick.
 
- carefully harness habit-forming properties of cannabis
  - when motivation to meditate is lacking, motivation to 'meditate on drugs' will likely remain (once it has been experienced).
  - use the substance, carefully linked, to help form the habit
  - slowly phase out the substance as success comes with smaller Ape-bribes (promised dosage)
  - Dharana will become easier to attain without chemical assistance
  - Ape buys into the plan, fires up the engine room in pursuit of higher gratification
  - only the habit remains
  - substance proves to be medicinal


* Footnotes

[fn:1]https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6333745/
